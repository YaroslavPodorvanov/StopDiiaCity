package main

import (
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"gitlab.com/stopdiiacity/stopdiiacity.netlify.app/verify"
)

func main() {
	// Make the handler available for Remote Procedure Call by AWS Lambda
	lambda.Start(func(r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
		var response = verify.Verify([]byte(r.Body))

		return events.APIGatewayProxyResponse{
			StatusCode: response.StatusCode,
			Headers:    verify.Headers(),
			Body:       response.Body,
		}, nil
	})
}
