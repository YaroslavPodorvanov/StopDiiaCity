module gitlab.com/stopdiiacity/stopdiiacity.netlify.app

go 1.14

require (
	github.com/aws/aws-lambda-go v1.24.0
	github.com/stretchr/testify v1.7.0
	github.com/valyala/quicktemplate v1.6.3
)
