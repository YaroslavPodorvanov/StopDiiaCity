package main

import (
	"gitlab.com/stopdiiacity/stopdiiacity.netlify.app/templates"
	"gitlab.com/stopdiiacity/stopdiiacity.netlify.app/verify"
	"log"
	"os"
)

func main() {
	f, err := os.OpenFile("./public/test.html", os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		log.Fatal(err)
	}

	templates.WriteGenerate(f, verify.Prefixes())

	if err := f.Close(); err != nil {
		log.Fatal(err)
	}
}
