package verify

import (
	"encoding/json"
	"github.com/stretchr/testify/require"
	"net/http"
	"testing"
)

func TestHandler(t *testing.T) {
	var (
		urls = []string{
			"https://jobs.dou.ua/companies/allright/reviews",
			"https://djinni.co/jobs/company-epam-systems-bb0df/",
		}
	)

	for _, url := range urls {
		var request = Request{
			URL:     url,
			Version: 1,
		}

		var body, marshalErr = json.Marshal(request)
		require.NoError(t, marshalErr)

		var response = Verify(body)
		require.Equal(t, Response{unsafeMessage, http.StatusOK}, response)
	}
}
